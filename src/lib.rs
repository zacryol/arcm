//! Provides a macro for more concise `Arc<Mutex<_>>` creation.

use std::sync::{Arc, Mutex};

/// Creates an `Arc<Mutex<_>` containing the expression.
///
/// # Examples
/// ```
/// # use arcm::arcm;
/// # use std::sync::{Arc, Mutex};
/// let x = arcm!(5);
/// # let y = x;
///
/// // is the same as
///
/// let x = Arc::new(Mutex::new(5));
/// # assert_eq!(*x.lock().unwrap(), *y.lock().unwrap());
/// ```
///
/// Type may also be specified if desired or needed.
///
/// ```
/// # use arcm::arcm;
/// # use std::sync::{Arc, Mutex};
/// let x = arcm!(5, u8);
/// # let y = x;
///
/// // is the same as
///
/// let x = Arc::new(Mutex::<u8>::new(5));
/// # assert_eq!(*x.lock().unwrap(), *y.lock().unwrap());
/// ```
#[macro_export]
macro_rules! arcm {
    ($x:expr) => {
        std::sync::Arc::new(std::sync::Mutex::new($x))
    };
    ($x:expr, $t:ty) => {
        std::sync::Arc::new(std::sync::Mutex::<$t>::new($x))
    };
}

/// Type alias for an `Arc<Mutex<_>>`.
///
/// If you want that too.
pub type ArcM<T> = Arc<Mutex<T>>;

