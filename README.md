# arcm

Macro for more concise `Arc<Mutex<_>>` creation.

`arcm!(42)` expands to `Arc::new(Mutex::new(42))`.

`arcm!(42, u8)` expands to `Arc::new(Mutex::<u8>::new(42))` for explicit type hints.

There's also an `ArcM` type alias.

And that's all.

## License

This code is available under the [WTFPL](https://spdx.org/licenses/WTFPL.html).
